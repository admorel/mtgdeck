package com.deck.magic.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CartesController {

	private JSONObject callAndReadRestAPI(String serviceToCall) throws IOException {

		CookieManager cm = new CookieManager();
		cm.setCookiePolicy(new ProxyAcceptCookiePolicy("scryfall.com"));
		CookieHandler.setDefault(cm);

		URL url = new URL("https://api.scryfall.com"+serviceToCall);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		int status = con.getResponseCode();

		BufferedReader in = null;

		if (status > 299) {
			in = new BufferedReader(
					new InputStreamReader(con.getErrorStream()));
		} else { 
			in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
		}

		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();

		JSONObject json = new JSONObject(content.toString());

		return json;

	}


	@RequestMapping("/cards")
	public ModelAndView getAllCard() throws IOException {
		ModelAndView mv = new ModelAndView("allcards");

		JSONObject response = callAndReadRestAPI("/cards");







		//mv.addObject("listCards", allCards);
		return mv;
	}


	@RequestMapping("/extension/{extension}")
	public ModelAndView getCardsExtension(HttpServletRequest httpServletRequest, 
			@PathVariable String extension,
			@RequestParam String... couleur) throws IOException {
		
		Locale currentLocale = httpServletRequest.getLocale();
		
		ModelAndView mv = new ModelAndView("extensioncards");

		JSONObject response = callAndReadRestAPI("/cards");

		// getting next page url 
		String next_page = (String) response.get("next_page");

		JSONArray listCardExtension = new JSONArray();

		while(!next_page.equals("https://api.scryfall.com/cards")) {

			// getting cards informations 
			JSONArray cards = (JSONArray) response.get("data"); 

			for (int i = 0; i < cards.length(); i++) {

				// getting cardSet
				String cardSet = (String) cards.getJSONObject(i).get("set");
				// getting card language
				String cardLang = (String) cards.getJSONObject(i).get("lang");

				if(cardSet.equals(extension) && currentLocale.getLanguage().equals(cardLang)) {
					
					// getting card color
					JSONArray cardColors = (JSONArray) response.get("colors");
					int indexCardColorCurrent = 0;
					boolean findOneColor = false;
					while(indexCardColorCurrent < cardColors.length() ||
							findOneColor == true) {
						
						for(int indexChoiceColorCurrent = 0; i < couleur.length; i++) {
							if(cardColors.get(indexCardColorCurrent).toString()
									.contains(couleur[indexChoiceColorCurrent])) {
								findOneColor = true;
								break;
							}
						}
						
						
					}
						
					if(findOneColor == true) listCardExtension.put(cards.getJSONObject(i));
				}

			}

			next_page = (String) response.get("next_page");
		}

		mv.addObject("listCards", listCardExtension);
		return mv;
	}



	@RequestMapping("/cards/{id}")
	public ModelAndView getOneCard(@PathVariable String id) {
		ModelAndView mv = new ModelAndView("card");


		//mv.addObject("card", card);
		return mv;
	}


	@RequestMapping("/cards/set/{setCode}")
	public ModelAndView getCardSet(@PathVariable String setCode) {
		ModelAndView mv = new ModelAndView("set");


		System.out.println("Test");


		//mv.addObject("cardsFiltered", cardsFiltered);
		return mv;
	}


}
