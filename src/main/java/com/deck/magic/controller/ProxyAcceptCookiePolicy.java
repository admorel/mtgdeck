package com.deck.magic.controller;

import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;

public class ProxyAcceptCookiePolicy implements CookiePolicy {
    private String acceptedProxy;
 
    public boolean shouldAccept(URI uri, HttpCookie cookie) {

        String host = null;
		try {
			host = InetAddress.getByName(uri.getHost())
			        .getCanonicalHostName();
			
	        if (HttpCookie.domainMatches(acceptedProxy, host)) {
	        	return true;
	        }
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
        return CookiePolicy.ACCEPT_ALL
          .shouldAccept(uri, cookie);
    }

	public ProxyAcceptCookiePolicy(String acceptedProxy) {
		super();
		this.acceptedProxy = acceptedProxy;
	}
 
    
}