<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Magic The Gathering all cards</title>
</head>
<body>
	<c:forEach items="${listCards}" var="card">
		<tr>
			${card.name}
		</tr>
	</c:forEach>


</body>
</html>